# dbot - BioMedIT dependency bot

This repo contains the `.gitlab-ci.yml` file and schedules needed to run the
**[Renovate](https://github.com/renovatebot/renovate)** dependency scanner on
selected **BioMedIT** projects.

**Renovate** is a dependency scanner that checks updates in dependencies. It
scans the package files (e.g. `package.json`) of assigned projects and creates
a new `MR` whenever an update in one of the project's dependencies is detected.

* [Renovate docs](https://docs.renovatebot.com)
* [Renovate config options](https://docs.renovatebot.com/configuration-options)
* [Renovate tutorial](https://github.com/renovatebot/tutorial)
* [Intro to Renovate blog post](https://blog.logrocket.com/renovate-dependency-updates-on-steroids)

## How Renovate works (in a nutshell)

**Renovate runs via a CI/CD job** that is hosted in the template of the public
[Renovate repo on GitLab](https://gitlab.com/renovate-bot/renovate-runner).

This template is added as an `include:` in the `.gitlab-ci.yml` file of the
present project. Running the CI/CD job of the present project will therefore
run **Renovate**.

* By default, **Renovate** pipeline **only runs on scheduled tasks**. This
  means that in order to use **Renovate**, a
  [scheduled task](https://gitlab.com/biomedit/tools/dbot/-/pipeline_schedules)
  must be added to the project hosting the `.gitlab-ci.yml` file that triggers
  the **Renovate** run (i.e. this project). See below for more details on how
  to set up a scheduled task.

  To run **Renovate** outside a scheduled task, the `renovate` job must be
  amended in the `.gitlab-ci.yml` file - here is an example:

  ```yaml
  include:
    - project: "renovate-bot/renovate-runner"
      file: "/templates/renovate.gitlab-ci.yml"

  renovate:
    rules:
      - if: '$CI_PIPELINE_SOURCE == "schedule"'
      - if: '$CI_PIPELINE_SOURCE == "push"'
  ```

* The projects to be scanned by **Renovate** are specified through the
  **`RENOVATE_EXTRA_FLAGS`** variable, which is set in the
  **[CICD > schedules](https://gitlab.com/biomedit/tools/dbot/-/pipeline_schedules)**
  section of the present repo.

### Ignoring an update - skipping a version

**Normal** Renovate MRs (as opposed to **Immortal** MRs) that have "uniqueness"
in their title relating to the version in the upgrade (i.e. the MR concerns a
single dependency) can simply be closed in order to ignore the update.

* **Important:** please note that ignoring a Renovate for a major/minor update
  will result in Renovate **ignoring all subsequent versions** for that given
  major/minor update. The reason for that is because:

  > Renovate uses the branch name and PR title like a cache key. If the same
  > key exists and the PR was closed, then we ignore the PR.

* **Examples:**

  * Closing `chore(deps): update dependency mypy to 1.1.23` will instruct
    Renovate to ignore only version `1.1.23`, but when version `1.1.24` comes
    around, Renovate will create a new MR for it.
  * Closing `chore(deps): update dependency mypy to 1.2` will ignore all mypy
    versions `1.2.x`, and will only re-create a new MR for version `1.3.x`.
  * Closing `chore(deps): update dependency prettier to v3` will ignore all
    versions `3.x.x`, and will only re-create a new MR for the next major
    release `v4`.
  * For more details see
    [here](https://docs.renovatebot.com/key-concepts/pull-requests/#normal-prs).

**Immortal** 👻 MRs cannot be silenced by closing them, because they are
always re-created. See [here for more info on Immortal PRs](https://docs.renovatebot.com/key-concepts/pull-requests/#immortal-prs)

## Setting-up Renovate for a new project

To add Renovate scanning to an existing project:

* **Add the BIWG bot** as a member of the project to scan. BIWG bot should have
  `Developer` permissions. Alternatively, the BIWG bot can also be added at
  the [group level](https://gitlab.com/groups/biomedit/-/group_members) so it
  has access to all the group's projects.

  _Reminder:_ To add a member to a project: **Project information > Members**.

* **Onboard the new project** to be scanned by **Renovate**. This onboarding needs
  to be run only once, and will create a `renovate.json` file at the root of
  the project to be scanned.

  Proceed as follows:

  1. Go to the [schedule page](https://gitlab.com/biomedit/tools/dbot/-/pipeline_schedules)
     of the present project.
  2. Click on `Edit` (pencil icon) of the `onboarding` scheduled task.  
     _Note:_ to be able to edit a scheduled task, you might need to
     **take ownership** of it.
  3. Set the `RENOVATE_EXTRA_FLAGS` variable to
     `--onboarding=true <project/path>`, where `<project/path>` is the path of
     the target project (i.e., the project to be scanned, e.g.
     `--onboarding=true biomedit/libbiomedit`).
  4. Save your changes and manually run the `onboarding` schedule job pipeline.
     This should create a new `MR` in the target project that adds a
     `renovate.json` config file to the target project.  
     **Important:** this pipeline must always be run manually (because
     onboarding needs to be performed only once per project), so make sure to
     leave the **Active** option unchecked.
     **Important:** when making changes to the `MR` created by **Renovate** (e.g. in
     points 5. and 6. below), **never amend the commits automatically created**
     by **Renovate**. Instead, any edits should be made in a new commit.
  5. Move the `renovate.json` file to `.gitlab/renovate.json` - this allows
     de-cluttering the root of the project. **Renovate** also searches for config
     files in `.gitlab/`, so the config file will be properly detected.
  6. Customize how **Renovate** should run in the target project by making changes
     to the `.gitlab/renovate.json` config file. For instance, `MR` for updates
     to dependencies can be grouped by language, so that all updates of
     packages for a given language are grouped in the same `MR`, rather than
     having 1 `MR` per dependency, as it the case by default.  
     Example: grouping dependencies MR by language with `packageRules` and
     `matchLanguages` options:

     ```yaml
     "packageRules":
       [
         { "matchLanguages": ["python"], "groupName": "python" },
         { "matchLanguages": ["docker"], "groupName": "docker" },
       ]
     ```

  7. When all edits are done and committed (remember to not edit the commits
     automatically made by Renovate), the onboarding MR can be merged.

* **Add the path of the project to scan** to the `main` pipeline schedule of
  the **dbot** project. This is done as follows:

  1. Go to the [schedule page](https://gitlab.com/biomedit/tools/dbot/-/pipeline_schedules)
     of the present project.
  2. Click on "Edit" (pencil icon) of the `main` scheduled task.
     _Note:_ to be able to edit a scheduled task, you might need to
     "take ownership" of it.
  3. Edit the `RENOVATE_EXTRA_FLAGS` variable of the schedule and add the path
     of the project to scan. For instance, to scan both `Portal` and
     `libbiomedit`, the variable content must be:

     ```sh
     biomedit/portal biomedit/libbiomedit
     ```

* **The project is now onboarded**, and will be scanned by Renovate during the
  next run of the tool, as specified in the `main` scheduled task.

## One-time set-up for Renovate

The steps below must be performed only once.

* Create a new project on GitLab whose CI/CD job will trigger the **Renovate**
  run in all onboarded projects. This is the present project.
* This project contains a single file, `.gitlab-ci.yml`, which itself contains
  basically only an `include:` of the official
  [Renovate repo on GitLab](https://gitlab.com/renovate-bot/renovate-runner).
* Add a **`RENOVATE_TOKEN`** variable to the **dbot** project (this project).
  The `RENOVATE_TOKEN` variable must be set to the **Personal Access Token**
  (PAT) of a bot account with permission as defined
  [here](https://docs.renovatebot.com/modules/platform/gitlab/#authentication)
  _Reminder:_ adding CI/CD variables to GitLab is done under
  **[Settings > CI/CD > Variables](https://gitlab.com/biomedit/tools/dbot/-/settings/ci_cd)**.
* Create a **`main`** and **`onboarding`** pipeline schedules. This is done
  under **[CI/CD > Schedules](https://gitlab.com/biomedit/tools/dbot/-/pipeline_schedules)**.

  * Both schedules should have a variable named `RENOVATE_EXTRA_FLAGS`, that
    contains the paths of the projects to scan:

    ```sh
    # Example of content for the "main" schedule.
    biomedit/portal biomedit/libbiomedit

    # Example of content for the "onboarding" schedule.
    --onboarding=true biomedit/libbiomedit
    ```

  * The **`onboarding`** schedule should be disabled (only runs on a manual
    trigger), while the **`main`** schedule should be set to run at fixed
    intervals. E.g. once a day (crontab: `30 7 * * 1-5`) or once a week.  
    _Tip:_ you can check your crontab value using [crontab guru](https://crontab.guru)

## Run frequency of Renovate

Currently, the `main` scheduled task is set to run **once a day** (crontab:
`30 7 * * 1-5`). This means that Renovate will check for updates in project
dependencies every day. If an update in a dependency is detected, a new `MR` is
created (the content of the `MR` is an increase in the version number of the
dependency).

Here is the rationale for running Renovate on a daily basis:

* **Running the Renovate pipeline frequently does _not_ create more MRs.** If
  an open `MR` already exist for the dependency to update (or a dependency part
  of the same dependency group), then the existing `MR` is updated (no new `MR`
  is created).
* Running **Renovate** every day does not mean that `MRs` have to be taken care
  of on a daily basis. The idea is to run Renovate daily, but only to merge the
  `MRs` maybe once a week.
* One advantage of running Renovate on a daily basis, is that between the time
  an `MR` is opened by Renovate and the time that someone starts working on it,
  there might already be a new version of the dependency (which might not be
  detected by then if Renovate runs only once a week).
* Currently, the limit is to create 2 `MRs` per project per hour. So if we only
  run it once a week, there might be some `MRs` that won't be created (unless
  we increase the limit).
